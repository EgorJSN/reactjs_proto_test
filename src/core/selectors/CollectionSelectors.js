import { createSelector } from "reselect";
import { process } from "@progress/kendo-data-query";
import { GRID_GROUP_CRITERIA } from "../reducers/CollectionReducer.js";

export const showEditDialogSelector = (state) => {
    return state.collections.showEditDialog;
}

export const getSelectedCollectionIdSelector = (state) => {
    return state.collections.selectedCollectionId;
}

export const getCollectionsSelector = (state) => {
    return state.collections.collections;
}

export const getEntitiesSelector = (state) => {
    return state.collections.entities;
}

export const getGroupedCollectionsSelector = createSelector(
    getCollectionsSelector,
    getSelectedCollectionIdSelector,
    collections => process(collections, GRID_GROUP_CRITERIA)
)