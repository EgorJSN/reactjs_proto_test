export const getCollectionEditSelector = (state) => {
    return state.collectionEdit.collection;
}

export const getContextsSelector = (state) => {
    return state.collectionEdit.contexts;
}

export const showEditFieldDialogSelector = (state) => {
    return state.collectionEdit.showEditFieldDialog;
}

export const getSelectedCustomFieldSelector = (state) => {
    return state.collectionEdit.selectedCustomField;
}