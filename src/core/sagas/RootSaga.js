import { all, fork } from 'redux-saga/effects';
import { watchCollectionList } from "./CollectionSaga.js";

export default function* rootSaga() {
    yield all([fork(watchCollectionList)]);
}
