import { call, put, takeEvery } from "redux-saga/effects";
import { COLLECTION_ACTION } from "../ActionTypes.js";
import {
    addCollectionAction, deleteCollectionAction,
    refreshCollectionAction,
    setCollectionsAction
} from "../action-creators/CollectionActionCreator.js";
import { deleteAPIData, fetchAPIData, postAPIData, updateAPIData } from "../../libraries/ApiHelper.js";
import { setContextsAction } from "../action-creators/CollectionEditActionCreator.js";
import { COLLECTION } from "../reducers/CollectionReducer.js";
import { showErrorAction } from "../action-creators/ErrorActionCreator.js";

const COLLECTIONS_DOWNLOAD_URL = 'collections';
const CONTEXTS_DOWNLOAD_URL = 'contexts';

const COLLECTIONS_UPDATE_URL = 'collection';
const COLLECTIONS_CREATE_URL = 'collection';
const COLLECTIONS_DELETE_URL = 'collection';

async function fetchCollections() {
    const response = await fetchAPIData(COLLECTIONS_DOWNLOAD_URL);
    return response.data;
}

function* downloadCollectionsWorker() {

    const fetchData = yield call(fetchCollections);
    const { collections, entities } = fetchData;

    yield put(setCollectionsAction(
        collections,
        entities,
    ));
}

async function fetchContexts() {
    const response = await fetchAPIData(CONTEXTS_DOWNLOAD_URL);
    return response.data['contexts'];
}

function* downloadContextsWorker() {

    const contexts = yield call(fetchContexts);

    yield put(setContextsAction(contexts));
}

async function updateCollection(collection) {

    const structure = collection.structure;

    if (structure.custom?.length) {
        structure.custom.forEach(f => delete f[COLLECTION.SPECIAL_SELECTED_FIELD]);
    }

    const collectionUpdateData = {
        ...collection,
        structure: JSON.stringify(structure),
    }

    const response = await updateAPIData(COLLECTIONS_UPDATE_URL, collectionUpdateData);
    return response.data['updatedCollection'];
}

function* updateCollectionWorker(action) {

    try {
        const updatedCollection = yield call(updateCollection, action.collection);
        yield put(refreshCollectionAction(updatedCollection));
    } catch (e) {
        const message = e.response?.data?.errors ?? e.response?.data ?? e.message;
        yield put(showErrorAction(message));
    }
}

async function createCollection(collection) {

    const response = await postAPIData(COLLECTIONS_CREATE_URL, {
        ...collection,
        structure: JSON.stringify(collection.structure),
    });
    return response.data['collection'];
}

function* createCollectionWorker(action) {

    try {
        const createdCollection = yield call(createCollection, action.collection);
        yield put(addCollectionAction(createdCollection));
    } catch (e) {
        const message = e.response?.data?.errors ?? e.response?.data ?? e.message;
        yield put(showErrorAction(message));
    }

}

async function deleteCollection(id) {

    await deleteAPIData(COLLECTIONS_DELETE_URL, { id });
}

function* deleteCollectionWorker(action) {

    yield call(deleteCollection, action.id);

    yield put(deleteCollectionAction());
}

export function* watchCollectionList() {

    yield takeEvery(COLLECTION_ACTION.DOWNLOAD, downloadCollectionsWorker);
    yield takeEvery(COLLECTION_ACTION.DOWNLOAD_CONTEXTS, downloadContextsWorker);

    yield takeEvery(COLLECTION_ACTION.UPDATE_COLLECTION, updateCollectionWorker);
    yield takeEvery(COLLECTION_ACTION.CREATE_COLLECTION, createCollectionWorker);
    yield takeEvery(COLLECTION_ACTION.DELETE_COLLECTION, deleteCollectionWorker);
}
