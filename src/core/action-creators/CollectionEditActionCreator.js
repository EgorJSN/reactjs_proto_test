import { COLLECTION_EDIT_ACTION } from "../ActionTypes.js";
import { COLLECTION } from "../reducers/CollectionReducer.js";

export const setCollectionEditFieldAction = (field, value) => {
    return {
        type: COLLECTION_EDIT_ACTION.SET_FIELD,
        field,
        value,
    }
}

export const showEditFieldDialogAction = (visible) => {
    return {
        type: COLLECTION_EDIT_ACTION.SHOW_EDIT_FIELD_DIALOG,
        visible,
    }
}

export const deleteCustomFieldAction = () => {
    return {
        type: COLLECTION_EDIT_ACTION.DELETE_CUSTOM_FIELD,
    }
}

export const updateCustomFieldAction = (customFieldData) => {
    return {
        type: COLLECTION_EDIT_ACTION.UPDATE_CUSTOM_FIELD,
        customFieldData,
    }
}

export const setContextsAction = (contexts) => {
    return {
        type: COLLECTION_EDIT_ACTION.SET_CONTEXTS,
        contexts,
    }
}

export const setSelectedCustomFieldAction = (selectedField) => {
    return {
        type: COLLECTION_EDIT_ACTION.SET_SELECTED_CUSTOM_FIELD,
        selectedField,
    }
}

export const setCollectionEditAction = (collections, selectedId) => {

    const collection = collections.find(collection => collection[COLLECTION.FIELD_ID] === selectedId);

    return {
        type: COLLECTION_EDIT_ACTION.SET,
        collection,
    };
}

export const setCollectionEditEntityAction = (collection) => {
    return {
        type: COLLECTION_EDIT_ACTION.SET,
        collection,
    };
}