import { COLLECTION_ACTION } from "../ActionTypes.js";

export const setSelectedCollectionIdAction = (collectionId) => {
    return {
        type: COLLECTION_ACTION.SET_SELECTED_COLLECTION_ID,
        collectionId,
    };
}

export const deleteCollectionAction = () => {
    return {
        type: COLLECTION_ACTION.DELETE,
    }
}

export const showEditCollectionDialogAction = (visible) => {
    return {
        type: COLLECTION_ACTION.SHOW_EDIT_DIALOG,
        visible,
    }
}

export const setCollectionsAction = (collections, entities) => {
    return {
        type: COLLECTION_ACTION.SET,
        collections,
        entities,
    };
}

export const refreshCollectionAction = (updatedCollection) => {
    return {
        type: COLLECTION_ACTION.REFRESH_COLLECTION,
        updatedCollection,
    };
}

export const addCollectionAction = (collection) => {
    return {
        type: COLLECTION_ACTION.ADD,
        collection,
    };
}

// ASYNC

export const downloadCollectionsAction = () => {
    return {
        type: COLLECTION_ACTION.DOWNLOAD,
    };
}

export const downloadContextsAction = () => {
    return {
        type: COLLECTION_ACTION.DOWNLOAD_CONTEXTS,
    };
}

export const updateCollectionAction = (collection) => {
    return {
        type: COLLECTION_ACTION.UPDATE_COLLECTION,
        collection,
    };
}

export const createCollectionAction = (collection) => {
    return {
        type: COLLECTION_ACTION.CREATE_COLLECTION,
        collection,
    };
}

export const asyncDeleteCollectionAction = (id) => {
    return {
        type: COLLECTION_ACTION.DELETE_COLLECTION,
        id,
    };
}