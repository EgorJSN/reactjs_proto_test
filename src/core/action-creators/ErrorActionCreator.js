import { ERROR_ACTION } from "../ActionTypes.js";

export const showErrorAction = (message) => {
    return {
        type: ERROR_ACTION.SHOW_ERRORS,
        message,
    }
}

export const hideErrorAction = () => {
    return {
        type: ERROR_ACTION.HIDE_ERRORS,
    }
}