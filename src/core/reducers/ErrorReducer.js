import { ERROR_ACTION } from "../ActionTypes.js";

const initialState = {
    message: null,
    visible: null,
};

const ErrorReducer = (state = initialState, action) => {

    switch (action.type) {
        case ERROR_ACTION.SHOW_ERRORS:

            let message = action.message;
            let formattedMessage;

            if (message instanceof Array) {
                formattedMessage = '';
                message.forEach((field) => {
                    formattedMessage += 'Field [' + field.field + ']: ' + field.message + '\n';
                });
            }

            return {
                ...state,
                message: formattedMessage ?? message,
                visible: true,
            }

        case ERROR_ACTION.HIDE_ERRORS:
            return {
                ...state,
                message: '',
                visible: false,
            }

        default:
            return {
                ...state,
            }
    }

}

export default ErrorReducer;