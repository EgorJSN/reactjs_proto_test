import { COLLECTION_ACTION } from "../ActionTypes.js";

export const COLLECTION = {
    FIELD_ID: 'ID',
    GROUPED_BY: 'context',
    SPECIAL_SELECTED_FIELD: '__selected',
};

export const GRID_GROUP_CRITERIA = {
    group: [{ field: COLLECTION.GROUPED_BY, },],
};

const initialState = {
    selectedCollectionId: null,
    showEditDialog: false,
    collections: [],
    entities: [],
};

export default function CollectionReducer(state = initialState, action) {

    switch (action.type) {
        case COLLECTION_ACTION.SET_SELECTED_COLLECTION_ID:
            return setSelectedCollectionId(state, action.collectionId);

        case COLLECTION_ACTION.SET:
            return setCollections(state, action.collections, action.entities);

        case COLLECTION_ACTION.DELETE:
            return deleteCollection(state);

        case COLLECTION_ACTION.SHOW_EDIT_DIALOG:
            return showEditDialog(state, action.visible);

        case COLLECTION_ACTION.REFRESH_COLLECTION:
            return refreshCollection(state, action.updatedCollection);

        case COLLECTION_ACTION.ADD:
            return addCollection(state, action.collection);

        default:
            return state;
    }
}

const addCollection = (state, collection) => {
    if (
        typeof collection.structure === 'string' ||
        collection.structure instanceof String
    ) {
        collection.structure = JSON.parse(collection.structure);
    }

    let collections = [...state.collections];
    collections.push(collection);

    return {
        ...state,
        collections,
    }
}

const refreshCollection = (state, updatedCollection) => {

    if (
        typeof updatedCollection.structure === 'string' ||
        updatedCollection.structure instanceof String
    ) {
        updatedCollection.structure = JSON.parse(updatedCollection.structure);
    }

    let collections = state.collections.map((collection) => {
        if (collection[COLLECTION.FIELD_ID] === updatedCollection[COLLECTION.FIELD_ID]) {
            return {
                ...collection,
                ...updatedCollection,
            };
        }
        return collection;
    });

    return {
        ...state,
        collections,
    }
}

const showEditDialog = (state, visible) => {
    if (state.showEditDialog === visible) {
        return {
            ...state
        };
    }

    return {
        ...state,
        showEditDialog: visible,
    }
}

const setSelectedCollectionId = (state, collectionId) => {
    state.collections.forEach((collection) => {
        collection[COLLECTION.SPECIAL_SELECTED_FIELD] = collection[COLLECTION.FIELD_ID] === collectionId;
    });

    return {
        ...state,
        selectedCollectionId: collectionId,
    };
}

const setCollections = (state, collections, entities) => {

    if (collections?.length) {
        collections.forEach((collection) => {
            collection.structure = collection.structure ? JSON.parse(collection.structure) : {};
            collection.structure.custom = collection.structure.custom ?? [];
        });
    }

    return {
        ...state,
        collections,
        entities,
    };
}

const deleteCollection = (state) => {

    if (!state.selectedCollectionId) {
        return state;
    }

    let collections = state.collections.filter(
        (collection) => collection[COLLECTION.FIELD_ID] !== state.selectedCollectionId
    );

    state.selectedCollectionId = null;

    return {
        ...state,
        collections: collections,
    };
}