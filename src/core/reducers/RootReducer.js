import { combineReducers } from 'redux';
import CollectionReducer from "./CollectionReducer.js";
import CollectionEditReducer from "./CollectionEditReducer.js";
import ErrorReducer from "./ErrorReducer.js";

const rootReducer = combineReducers({
    collections: CollectionReducer,
    collectionEdit: CollectionEditReducer,
    errors: ErrorReducer,
});

export default rootReducer;