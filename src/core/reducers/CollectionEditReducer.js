import { COLLECTION_EDIT_ACTION } from "../ActionTypes.js";
import { COLLECTION } from "./CollectionReducer.js";

export const initialState = {
    collection: {
        name: null,
        context: null,
        context_id: null,
        structure: { custom: [] },
    },
    contexts: [],
    selectedCustomField: {},
    showEditFieldDialog: false,
};

const CollectionEditReducer = (state = initialState, action) => {
    switch (action.type) {
        case COLLECTION_EDIT_ACTION.SET:
            return setCollectionEdit(state, action.collection);

        case COLLECTION_EDIT_ACTION.SET_FIELD:
            return setCollectionEditField(state, action.field, action.value);

        case COLLECTION_EDIT_ACTION.SET_CONTEXTS:
            return setContexts(state, action.contexts);

        case COLLECTION_EDIT_ACTION.SET_SELECTED_CUSTOM_FIELD:
            return setSelectedCustomField(state, action.selectedField);

        case COLLECTION_EDIT_ACTION.SHOW_EDIT_FIELD_DIALOG:
            return showEditFieldDialog(state, action.visible);

        case COLLECTION_EDIT_ACTION.UPDATE_CUSTOM_FIELD:
            return customFieldUpdate(state, action.customFieldData);

        case COLLECTION_EDIT_ACTION.DELETE_CUSTOM_FIELD:
            return deleteCustomField(state);

        default:
            return state;
    }
}

const showEditFieldDialog = (state, visible) => {
    return {
        ...state,
        showEditFieldDialog: visible,
    }
}

const deleteCustomField = (state) => {
    if (!state.selectedCustomField.name) {
        return {
            ...state,
        }
    }

    let collection = { ...state.collection };

    let customFields = collection.structure.custom.map((field) => {
        field[COLLECTION.SPECIAL_SELECTED_FIELD] = false;
        return field;
    });

    customFields = customFields.filter(
        field => field.name !== state.selectedCustomField.name
    )

    collection.structure.custom = customFields;

    return {
        ...state,
        collection,
        selectedCustomField: {},
    }
}

const customFieldUpdate = (state, customFieldData) => {
    const collection = { ...state.collection };

    let updateField = collection.structure.custom.find(
        field => field.name === state.selectedCustomField.name
    );

    let selectedCustomField = {};

    // add new field
    if (!updateField) {
        const checkNameIsExist = collection.structure.custom.find(
            field => field.name === customFieldData.name
        );

        if (!checkNameIsExist) {
            collection.structure.custom.push({ ...customFieldData });
        }
        // update field
    } else {
        selectedCustomField = customFieldData;
        collection.structure.custom = collection.structure.custom.map(
            field => field.name === state.selectedCustomField.name ? customFieldData : field
        );
    }

    return {
        ...state,
        collection,
        selectedCustomField,
    }
}

const setSelectedCustomField = (state, selectedField) => {

    if (state.selectedCustomField === selectedField) {
        return {
            ...state,
        }
    }

    let customFields;

    // add new field
    if (!selectedField) {
        selectedField = {};
        customFields = state.collection.structure.custom.map((field) => {
            field[COLLECTION.SPECIAL_SELECTED_FIELD] = false;
            return field;
        });
        // edit field
    } else {
        customFields = state.collection.structure.custom.map((field) => {
            field[COLLECTION.SPECIAL_SELECTED_FIELD] = field.name === selectedField.name;
            return field;
        });
    }

    let collection = { ...state.collection };
    collection.structure.custom = customFields;

    return {
        ...state,
        collection,
        selectedCustomField: selectedField,
    }
}

const setCollectionEditField = (state, field, value) => {
    let newCollection = {
        ...state.collection,
        [field]: value,
    }

    return {
        ...state,
        collection: newCollection,
    }
}

const setContexts = (state, contexts) => {
    return {
        ...state,
        contexts,
    }
}

const setCollectionEdit = (state, collection) => {
    return {
        ...state,
        collection,
    }
}

export default CollectionEditReducer;