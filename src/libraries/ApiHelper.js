import axios from "axios";

export const fetchAPIData = async (url) => {
    return await axios.get(`${ process.env.REACT_APP_BASE_API_URL }${ url }`);
}

export const postAPIData = async (url, data) => {
    return await axios.post(`${ process.env.REACT_APP_BASE_API_URL }${ url }`, data);
}

export const updateAPIData = async (url, data) => {
    return await axios.put(`${ process.env.REACT_APP_BASE_API_URL }${ url }`, data);
}

export const deleteAPIData = async (url, data) => {
    return await axios.delete(`${ process.env.REACT_APP_BASE_API_URL }${ url }`, { data });
}