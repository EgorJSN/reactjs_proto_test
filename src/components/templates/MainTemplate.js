import * as React from 'react';
import '@progress/kendo-theme-default/dist/all.css';
import '../../style.css';
import CollectionPage from "../pages/CollectionPage.js";

const MainTemplate = () => {

    return (
        <CollectionPage/>
    );
}

export default MainTemplate;