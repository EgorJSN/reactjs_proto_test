import * as React from "react";
import styles from "../../molecules/collection-fields.module.scss";
import { useDispatch } from "react-redux";
import { setSelectedCustomFieldAction } from "../../../core/action-creators/CollectionEditActionCreator.js";

const GridCellEntity = (props) => {

    const dispatch = useDispatch();
    const value = props.dataItem[props.field];
    const type = props.dataItem.type;

    const cellClickHandler = () => {
        dispatch(setSelectedCustomFieldAction(props.dataItem));
    }

    return (
        <td className={styles['entity-collection-field']} onClick={cellClickHandler}>
            <div>
                { type === 'collection' && (
                    <span className="icon-toolbar icon-collection"></span>
                ) }
                { type === 'entity' && (
                    <span className="icon-toolbar icon-entity"></span>
                ) }
                <span>{value}</span>
            </div>
        </td>
    );
}

export default GridCellEntity;