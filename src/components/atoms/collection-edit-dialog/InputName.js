import * as React from "react";
import { Label } from "@progress/kendo-react-labels";
import { useDispatch, useSelector } from "react-redux";
import { setCollectionEditFieldAction } from "../../../core/action-creators/CollectionEditActionCreator.js";
import { Input } from "@progress/kendo-react-inputs";
import { getCollectionEditSelector } from "../../../core/selectors/CollectionEditSelectors.js";

const idInput = 'input-field-' + Date.now().toString();

const InputName = () => {

    const dispatch = useDispatch();

    let collectionEdit = useSelector(getCollectionEditSelector);

    const onChange = (e) => {
        dispatch(setCollectionEditFieldAction('name', e.target.value));
    };

    return (
        <div>
            <Label editorId={ idInput }>Name</Label>
            <Input id={ idInput } defaultValue={ collectionEdit.name } onChange={ onChange }/>
        </div>
    );
}

export default InputName;