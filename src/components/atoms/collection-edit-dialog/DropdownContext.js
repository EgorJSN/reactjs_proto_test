import * as React from "react";
import { DropDownList } from "@progress/kendo-react-dropdowns";
import { Label } from "@progress/kendo-react-labels";
import { useDispatch, useSelector } from "react-redux";
import { getCollectionEditSelector, getContextsSelector } from "../../../core/selectors/CollectionEditSelectors.js";
import { setCollectionEditFieldAction } from "../../../core/action-creators/CollectionEditActionCreator.js";

const idInput = 'dd-field-' + Date.now().toString();

const DropdownContext = () => {

    const collection = useSelector(getCollectionEditSelector);
    const contexts = useSelector(getContextsSelector);
    const dispatch = useDispatch();

    const initialValue = {
        id: collection.context_id,
        name: collection.context,
    };

    const handleChange = event => {
        const selectedContext = event.target.value;

        dispatch(setCollectionEditFieldAction('context_id', selectedContext.id));
        dispatch(setCollectionEditFieldAction('context', selectedContext.name));
    };

    return <div>
        <Label editorId={idInput}>
            Contexts
        </Label>

        <DropDownList
            id={idInput}
            style={ { zIndex: 999999 } }
            data={ contexts }
            textField={ 'name' }
            dataItemKey={ 'id' }
            defaultValue={ initialValue }
            onChange={ handleChange }
        />
    </div>;
};

export default DropdownContext;