import * as React from 'react';
import CollectionsList from "../organisms/CollectionsList.js";
import AddCollectionDialog from "../organisms/AddCollectionDialog.js";
import Dialog from "../molecules/custom-field-dialog/Dialog.js";
import ErrorDialog from "../molecules/ErrorDialog.js";

const CollectionPage = () => {
    return (
        <>
            <CollectionsList/>
            <AddCollectionDialog/>
            <Dialog/>
            <ErrorDialog/>
        </>
    );
};

export default CollectionPage;