import { useDispatch, useSelector } from "react-redux";
import { getCollectionEditSelector } from "../../../core/selectors/CollectionEditSelectors.js";
import { Checkbox } from "@progress/kendo-react-inputs";
import CustomFieldsSection from "./CustomFieldsSection.js";
import styles from "../collection-fields.module.scss";
import { setCollectionEditFieldAction } from "../../../core/action-creators/CollectionEditActionCreator.js";

const DefaultFieldsSection = () => {

    const dispatch = useDispatch();
    const collection = useSelector(getCollectionEditSelector);
    const fields = collection.structure;

    const onFieldChange = (e) => {
        const fieldName = e.target.element.name;
        const value = e.value;

        const newStructure = {
            ...collection.structure,
            [fieldName]: value,
        };

        dispatch(setCollectionEditFieldAction('structure', newStructure));
    }

    return (
        <div className={ styles['collection-fields-wrapper'] }>

            <div className={ styles['default-fields'] }>

                <label>Default fields</label>

                <Checkbox defaultChecked={ true } disabled={ true } label={ "Name" }/>

                <Checkbox
                    name={ 'code' }
                    defaultChecked={ fields.code }
                    label={ "Code" }
                    onChange={ onFieldChange }
                />

                <Checkbox
                    name={ 'description' }
                    defaultChecked={ fields.description }
                    label={ "Description" }
                    onChange={ onFieldChange }
                />

                <Checkbox
                    name={ 'sort' }
                    defaultChecked={ fields.sort }
                    label={ "Sort" }
                    onChange={ onFieldChange }
                />

            </div>

            <CustomFieldsSection/>
        </div>
    )
}

export default DefaultFieldsSection;