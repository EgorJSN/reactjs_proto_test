import { getSelectedState, Grid, GridColumn, GridToolbar } from "@progress/kendo-react-grid";
import { COLLECTION } from "../../../core/reducers/CollectionReducer.js";
import * as React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
    getCollectionEditSelector,
    getSelectedCustomFieldSelector
} from "../../../core/selectors/CollectionEditSelectors.js";
import styles from "../collection-fields.module.scss";
import GridCellEntity from "../../atoms/collection-edit-dialog/GridCellEntity.js";
import CrudButtonsPanel from "../CrudButtonsPanel.js";
import {
    deleteCustomFieldAction,
    setSelectedCustomFieldAction,
    showEditFieldDialogAction
} from "../../../core/action-creators/CollectionEditActionCreator.js";

const CustomFieldsSection = () => {

    const dispatch = useDispatch();
    const collection = useSelector(getCollectionEditSelector);
    const selectedField = useSelector(getSelectedCustomFieldSelector);
    const fields = collection.structure?.custom ?? [];

    const onSelectionChange = (gridEvent) => {
        let selectedItem = getSelectedState({
            event: gridEvent,
            dataItemKey: 'name',
        });

        const selectedItemKey = Object.keys(selectedItem)[0];
        let selectedItemData = gridEvent.dataItems.find(i => i.name === selectedItemKey);

        dispatch(setSelectedCustomFieldAction(selectedItemData));
    };

    const onEditField = () => {
        dispatch(showEditFieldDialogAction(true));
    }

    const onDeleteField = () => {
        dispatch(deleteCustomFieldAction());
    }

    const onAddField = async () => {
        await dispatch(setSelectedCustomFieldAction(null));
        await dispatch(showEditFieldDialogAction(true));
    }

    return (
        <Grid
            className={ styles['custom-fields'] }
            resizable={ false }
            reorderable={ false }
            selectable={ {
                enabled: true,
                drag: false,
                cell: false,
                mode: 'single',
            } }

            dataItemKey={ 'name' }
            selectedField={ COLLECTION.SPECIAL_SELECTED_FIELD }
            onSelectionChange={ onSelectionChange }

            data={ fields }
        >
            <GridToolbar>
                <div className={ styles['toolbar'] }>
                    <span className={ styles['title'] }>Custom Fields</span>

                    <CrudButtonsPanel
                        createHandler={ onAddField }
                        updateHandler={ onEditField }
                        deleteHandler={ onDeleteField }

                        updateDisabled={ !selectedField?.name }
                        deleteDisabled={ !selectedField?.name }
                    />
                </div>
            </GridToolbar>

            <GridColumn field="name" title="Name"/>
            <GridColumn field="name2" title="Entity / Collection" cell={ GridCellEntity }></GridColumn>
        </Grid>
    );
}

export default CustomFieldsSection;