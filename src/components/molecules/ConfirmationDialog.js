import * as React from 'react';
import { Dialog, DialogActionsBar } from '@progress/kendo-react-dialogs';

const ConfirmationDialog = (props) => {

    return <>
        { props.visible && <Dialog title={ props.title } onClose={ props.onClose }>
            <p style={ {
                margin: "25px",
                textAlign: "center"
            } }>{ props.description }</p>
            <DialogActionsBar>
                <button className="k-button" onClick={ props.onClose }>No</button>
                <button className="k-button" onClick={ props.onSuccess }>Yes</button>
            </DialogActionsBar>
        </Dialog> }
    </>;
}

export default ConfirmationDialog;