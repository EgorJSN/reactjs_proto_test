import ButtonGroup from "@progress/kendo-react-buttons/dist/es/ButtonGroup.js";
import styles from "./collection-fields.module.scss";
import Button from "@progress/kendo-react-buttons/dist/es/Button.js";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faMinus, faPlus } from "@fortawesome/free-solid-svg-icons";
import * as React from "react";

const CrudButtonsPanel = (props) => {

    const emptyHandler = () => {
    };
    const createHandler = props.createHandler || emptyHandler;
    const deleteHandler = props.deleteHandler || emptyHandler;
    const updateHandler = props.updateHandler || emptyHandler;

    const createDisabled = props.createDisabled || false;
    const deleteDisabled = props.deleteDisabled || false;
    const updateDisabled = props.updateDisabled || false;

    return (
        <ButtonGroup className={ styles['controls'] }>

            <Button
                look={ "flat" }
                onClick={ createHandler }
                disabled={ createDisabled }
            >
                <FontAwesomeIcon icon={ faPlus }/>
            </Button>

            <Button
                look={ "flat" }
                onClick={ deleteHandler }
                disabled={ deleteDisabled }
            >
                <FontAwesomeIcon icon={ faMinus }/>
            </Button>

            <Button
                look={ "flat" }
                onClick={ updateHandler }
                disabled={ updateDisabled }
            >
                <FontAwesomeIcon icon={ faEdit }/>
            </Button>

        </ButtonGroup>
    );
}

export default CrudButtonsPanel;