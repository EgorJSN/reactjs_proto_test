import * as React from 'react';
import { Dialog, DialogActionsBar } from '@progress/kendo-react-dialogs';
import { useDispatch, useSelector } from "react-redux";
import { getErrorsSelector } from "../../core/selectors/ErrorSelectors.js";
import { hideErrorAction } from "../../core/action-creators/ErrorActionCreator.js";

const ErrorDialog = () => {

    const dispatch = useDispatch();
    let errors = useSelector(getErrorsSelector);

    const onClose = () => dispatch(hideErrorAction());

    return <>
        { errors.visible && <Dialog title={ 'Error' } onClose={ onClose }>

            <p style={ {
                margin: "25px",
            } }>
                <pre>
                    { errors.message }
                </pre>
            </p>

            <DialogActionsBar>
                <button className="k-button" onClick={ onClose }>Ok</button>
            </DialogActionsBar>

        </Dialog> }
    </>;
}

export default ErrorDialog;