import { Window, WindowActionsBar } from "@progress/kendo-react-dialogs";
import { useDispatch, useSelector } from "react-redux";
import {
    getSelectedCustomFieldSelector,
    showEditFieldDialogSelector
} from "../../../core/selectors/CollectionEditSelectors.js";
import { Button } from "@progress/kendo-react-buttons";
import styles from "../../organisms/collection-dialog.module.scss";
import fieldsStyles from "../collection-fields.module.scss";
import * as React from "react";
import {
    showEditFieldDialogAction,
    updateCustomFieldAction
} from "../../../core/action-creators/CollectionEditActionCreator.js";
import { Label } from "@progress/kendo-react-labels";
import { Input } from "@progress/kendo-react-inputs";
import { useEffect, useState } from "react";
import { DropDownList } from "@progress/kendo-react-dropdowns";
import { getCollectionsSelector, getEntitiesSelector } from "../../../core/selectors/CollectionSelectors.js";

const DD_TYPES = [
    {
        key: 'collection',
        name: 'Collection',
    },
    {
        key: 'entity',
        name: 'Entity',
    },
];

const Dialog = () => {

    let visible = useSelector(showEditFieldDialogSelector);

    const dispatch = useDispatch();
    let editField = useSelector(getSelectedCustomFieldSelector);
    let entities = useSelector(getEntitiesSelector);
    let collections = useSelector(getCollectionsSelector);

    const onClose = () => dispatch(showEditFieldDialogAction(false));

    const onUpdateField = () => {
        dispatch(showEditFieldDialogAction(false));
        dispatch(updateCustomFieldAction(customFieldState));
    };

    const [customFieldState, setCustomFieldState] = useState();

    useEffect(() => {
        if (!editField.name && collections.length) {
            setCustomFieldState({
                name: 'field name',
                type: 'collection',
                name2: collections[0].name,
                id: collections[0].ID,
            });
        } else {
            setCustomFieldState(editField);
        }
    }, [editField, collections]);

    const initialTypeValue = customFieldState?.type === 'collection' ? DD_TYPES[0] : DD_TYPES[1];

    const onNameUpdate = (e) => {
        setCustomFieldState({
            ...customFieldState,
            name: e.target.value,
        });
    };

    const onName2Update = (e) => {
        const value = e.target.value;
        const id = value.id ?? value.ID;

        setCustomFieldState({
            ...customFieldState,
            name2: value.name,
            id,
        });
    };

    const onFieldTypeChange = (e) => {
        setCustomFieldState({
            ...customFieldState,
            type: e.target.value.key,
        })
    };

    return (
        <>
            { visible && (
                <Window
                    title={ `${editField?.name ? 'Edit' : 'Add'} Field` }
                    initialWidth={ 300 }
                    initialHeight={ 270 }
                    resizable={ false }
                    initialTop={ 150 }
                    modal={ true }
                    onClose={ onClose }
                >

                    <div className={ fieldsStyles['dialog-custom-field-wrapper'] }>

                        <div>
                            <Label editorId={ 'custom_field_name' }>Name </Label>
                            <Input id={ 'custom_field_name' } defaultValue={ customFieldState?.name ?? 'enter name' }
                                   onChange={ onNameUpdate }/>
                        </div>

                        <div>
                            <Label editorId={ 'custom_field_type' }>Type </Label>
                            <DropDownList
                                id={ 'custom_field_type' }
                                style={ { zIndex: 999999 } }
                                data={ DD_TYPES }
                                textField={ 'name' }
                                dataItemKey={ 'key' }
                                defaultValue={ initialTypeValue }
                                onChange={ onFieldTypeChange }
                            />
                        </div>

                        { customFieldState.type === 'entity' && (
                            <div>
                                <Label editorId={ 'custom_field_entity' }>Entity </Label>
                                <DropDownList
                                    id={ 'custom_field_entity' }
                                    style={ { zIndex: 999999 } }
                                    data={ entities }
                                    textField={ 'name' }
                                    dataItemKey={ 'id' }
                                    defaultValue={ entities[0] }
                                    onChange={ onName2Update }
                                />
                            </div>
                        ) }

                        { customFieldState.type === 'collection' && (
                            <div>
                                <Label editorId={ 'custom_field_collection' }>Collection </Label>
                                <DropDownList
                                    id={ 'custom_field_collection' }
                                    style={ { zIndex: 999999 } }
                                    data={ collections }
                                    textField={ 'name' }
                                    dataItemKey={ 'ID' }
                                    defaultValue={ collections[0] }
                                    onChange={ onName2Update }
                                />
                            </div>
                        ) }

                    </div>

                    <WindowActionsBar layout={ 'end' }>
                        <Button className={ styles['window-button'] } primary={ false } onClick={ onClose }>
                            Cancel
                        </Button>

                        <Button className={ styles['window-button'] } primary={ true } onClick={ onUpdateField }>
                            Ok
                        </Button>
                    </WindowActionsBar>

                </Window>
            ) }
        </>
    );
}

export default Dialog;