import * as React from 'react';
import { getSelectedState, Grid, GridColumn, GridToolbar } from '@progress/kendo-react-grid';
import { useDispatch, useSelector } from "react-redux";
import {
    getCollectionsSelector,
    getGroupedCollectionsSelector, getSelectedCollectionIdSelector,
} from "../../core/selectors/CollectionSelectors.js";
import {
    asyncDeleteCollectionAction,
    downloadCollectionsAction, downloadContextsAction, setSelectedCollectionIdAction, showEditCollectionDialogAction
} from "../../core/action-creators/CollectionActionCreator.js";
import { COLLECTION, GRID_GROUP_CRITERIA } from "../../core/reducers/CollectionReducer.js";
import { useEffect, useState } from "react";
import {
    setCollectionEditAction, setCollectionEditEntityAction
} from "../../core/action-creators/CollectionEditActionCreator.js";
import CrudButtonsPanel from "../molecules/CrudButtonsPanel.js";
import { initialState } from "../../core/reducers/CollectionEditReducer.js";
import ConfirmationDialog from "../molecules/ConfirmationDialog.js";

const CollectionList = () => {

    const dispatch = useDispatch();

    let groupedCollections = useSelector(getGroupedCollectionsSelector);
    let collections = useSelector(getCollectionsSelector);
    let selectedCollectionId = useSelector(getSelectedCollectionIdSelector);

    let [deleteConfirmation, setDeleteConfirmation] = useState(false);

    useEffect(() => {
        dispatch(downloadCollectionsAction());
        dispatch(downloadContextsAction());
    }, [dispatch]);

    const onClickAddCollection = () => {
        dispatch(setCollectionEditEntityAction(initialState.collection));
        dispatch(setSelectedCollectionIdAction(null));
        dispatch(showEditCollectionDialogAction(true));
    };

    const onClickDeleteCollection = () => {
        setDeleteConfirmation(true);
    };

    const onClickEditCollection = () => dispatch(showEditCollectionDialogAction(true));

    const onSelectionChange = (gridEvent) => {
        let selectedItem = getSelectedState({
            event: gridEvent,
            dataItemKey: COLLECTION.FIELD_ID,
        });

        const selectedItemId = parseInt(Object.keys(selectedItem)[0]);
        dispatch(setSelectedCollectionIdAction(selectedItemId));
        dispatch(setCollectionEditAction(collections, selectedItemId));
    };

    return (
        <>
            <Grid
                resizable={ true }
                reorderable={ true }
                groupable={ true }
                selectable={ {
                    enabled: true,
                    drag: false,
                    cell: false,
                    mode: 'single',
                } }

                dataItemKey={ COLLECTION.FIELD_ID }
                selectedField={ COLLECTION.SPECIAL_SELECTED_FIELD }
                onSelectionChange={ onSelectionChange }

                expandField="expanded"

                { ...GRID_GROUP_CRITERIA }
                data={ groupedCollections }
            >
                <GridToolbar>
                    <span className="icon-toolbar icon-collection"></span>

                    <CrudButtonsPanel
                        createHandler={ onClickAddCollection }
                        deleteHandler={ onClickDeleteCollection }
                        updateHandler={ onClickEditCollection }

                        updateDisabled={ !selectedCollectionId }
                        deleteDisabled={ !selectedCollectionId }
                    />
                </GridToolbar>

                <GridColumn field="name" title="Name"/>
                <GridColumn field="status" title="Status"/>
            </Grid>

            <ConfirmationDialog
                visible={ deleteConfirmation }
                title={ 'Delete collection' }
                description={ 'Are you sure you want to delete the collection?' }

                onClose={ () => setDeleteConfirmation(false) }
                onSuccess={ () => {
                    setDeleteConfirmation(false);
                    dispatch(asyncDeleteCollectionAction(selectedCollectionId))
                } }
            />
        </>
    )
};

export default CollectionList;
