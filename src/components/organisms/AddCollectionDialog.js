import * as React from 'react';
import { Window, WindowActionsBar } from "@progress/kendo-react-dialogs";
import { useDispatch, useSelector } from "react-redux";
import { getSelectedCollectionIdSelector, showEditDialogSelector } from "../../core/selectors/CollectionSelectors.js";
import {
    createCollectionAction,
    showEditCollectionDialogAction,
    updateCollectionAction
} from "../../core/action-creators/CollectionActionCreator.js";
import { Button } from "@progress/kendo-react-buttons";
import { FieldWrapper } from "@progress/kendo-react-form";
import styles from './collection-dialog.module.scss';
import DropdownContext from "../atoms/collection-edit-dialog/DropdownContext.js";
import InputName from "../atoms/collection-edit-dialog/InputName.js";
import { setCollectionEditFieldAction } from "../../core/action-creators/CollectionEditActionCreator.js";
import { getCollectionEditSelector } from "../../core/selectors/CollectionEditSelectors.js";
import DefaultFieldsSection from "../molecules/collection-dialog/DefaultFieldsSection.js";
import { TextArea } from "@progress/kendo-react-inputs";

const AddCollectionDialog = () => {

    let visible = useSelector(showEditDialogSelector);
    let editCollectionItem = useSelector(getCollectionEditSelector);
    let editCollectionId = useSelector(getSelectedCollectionIdSelector);

    let dispatch = useDispatch();

    const onClose = () => dispatch(showEditCollectionDialogAction(false));

    const onUpdateCollection = () => {
        if (editCollectionItem.ID) {
            dispatch(updateCollectionAction(editCollectionItem));
        } else {
            dispatch(createCollectionAction(editCollectionItem));
        }

        dispatch(showEditCollectionDialogAction(false));
    }

    const onDescriptionChange = (e) => {
        dispatch(setCollectionEditFieldAction('description', e.value));
    }

    const title = editCollectionId ? `Edit Collection: ${ editCollectionItem.name }` : 'Add Collection';

    return (
        <>
            { visible && (
                <Window
                    title={ title }
                    initialWidth={ 550 }
                    initialHeight={ 560 }
                    onClose={ onClose }
                    resizable={ false }
                    initialTop={ 50 }
                    modal={ true }
                >

                    <div className={ styles['field-row'] }>

                        <FieldWrapper className={ styles['field'] }>
                            <InputName/>
                        </FieldWrapper>

                        <FieldWrapper className={ styles['field'] }>
                            <DropdownContext/>
                        </FieldWrapper>
                    </div>

                    <DefaultFieldsSection/>

                    <p>Description:</p>
                    <TextArea
                        rows={ 3 }
                        style={ { width: '100%' } }
                        onChange={ onDescriptionChange }
                        defaultValue={ editCollectionItem.description ?? '' }
                    />

                    <WindowActionsBar layout={ 'end' }>
                        <Button className={ styles['window-button'] } primary={ false } onClick={ onClose }>
                            Cancel
                        </Button>

                        <Button className={ styles['window-button'] } primary={ true } onClick={ onUpdateCollection }>
                            Ok
                        </Button>
                    </WindowActionsBar>

                </Window>
            ) }
        </>
    );
}

export default AddCollectionDialog;